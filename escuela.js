function Alumno(id, num_ctrl, nombre, ape_pat, ape_mat, genero){
	return {
		
		id : id,
		num_ctrl : num_ctrl,
		"nombre" : nombre,
		"ape_pat" : ape_pat,
		"ape_mat" : ape_mat,
		"genero" : genero
	};
}


function Grupo(id, clave, nombre, alumnos, docentes, materias){
	return {
		id: id,
		"clave" : clave,
		"nombre" : nombre,
		alumnos: Alumno([29]),
		docentes: Docente([6]),
		materias: Materia([6])
	};
}


function Docente(id, rfc, nombre, ape_pat, ape_mat, genero){
	return {
		id:id,
		"rfc":rfc,
		"nombre":nombre,
		"ape_pat":ape_pat,
		"ape_mat":ape_mat,
		"genero":genero
	};
}


function Materia(id, clave, nombre){
	return {
		id:id,
		"clave":clave,
		"nombre":nombre
	};
}


